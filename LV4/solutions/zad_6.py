# Zadatak 6
# Za primjer 4.2. iz priloga koristite veći broj dodatnih veličina u modelu (npr., degree=15). Međutim, umjesto obične
# linearne regresije koristite Ridge regresiju. Mijenjate vrijednost regularizacijskog parametar. Što primjećujete? Kako
# glase koeficijenti ovog modela, a kako modela iz zadatka 5 za degree = 15. Komentirajte dobivene rezultate. 