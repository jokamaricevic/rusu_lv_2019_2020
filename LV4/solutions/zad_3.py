# Zadatak 3. Na podacima iz zadatka 1 potrebno je odrediti parametre linearnog modela na podacima za učenje na način da se
# implementira funkcija za izračunavanje parametara linearnog modela metodom gradijentnog spusta. Usporedite dobivene
# parametre modela s vrijednostima parametara linearnog modela iz zadatka 1.
# Slikom prikažite vrijednosti kriterijske funkcije u svakoj iteraciji algoritma. Mijenjate duljinu koraka α od vrlo malih
# vrijednosti do vrlo velikih vrijednosti. Što se događa? 

import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error

def non_func(x):
	y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
	return y

def add_noise(y):
    np.random.seed(14)
    varNoise = np.max(y) - np.min(y)
    y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
    return y_noisy

x = np.linspace(1,10,100)
y_true = non_func(x)
y_measured = add_noise(y_true)

np.random.seed(12)
indeksi = np.random.permutation(len(x))
indeksi_train = indeksi[0:int(np.floor(0.7*len(x)))]
indeksi_test = indeksi[int(np.floor(0.7*len(x)))+1:len(x)]

x = x[:, np.newaxis]
y_measured = y_measured[:, np.newaxis]

xtrain = x[indeksi_train]
ytrain = y_measured[indeksi_train]

xtest = x[indeksi_test]
ytest = y_measured[indeksi_test]

linearModel = lm.LinearRegression()
linearModel.fit(xtrain,ytrain)

ytest_p = linearModel.predict(xtest)
MSE_test = mean_squared_error(ytest, ytest_p)


n=len(xtrain)
def pravac(x, theta0, theta1):
    return theta0 + theta1*x

alfa = 0.01
theta = [0,0]
sve_udalj=[]

for i in range(0,5000):
    sumat1=0
    sumat2=0
    udalj=0
    for j in range (0, n):
        sumat1= sumat1 + (pravac(xtrain[j], theta[0], theta[1])-ytrain[j])*xtrain[j]/len(xtrain)
        sumat2= sumat2 + (pravac(xtrain[j], theta[0], theta[1])-ytrain[j])/len(xtrain)
        udalj=udalj + abs(ytrain[j]-theta[0]-theta[1]*xtrain[j])  #suma udaljenosti svake točke od pravca po y osi
    sve_udalj.append(udalj) #sprema tu udaljenost u vektor svih udaljenosti [za sve iteracije]
    theta[0] =theta[0] - alfa*sumat2   
    theta[1] =theta[1] - alfa*sumat1

        
plt.figure(4)
plt.plot(sve_udalj[:8])
print ('Model je oblika y_hat = Theta0 + Theta1 * x')
print ('y_hat = ', theta[0], '+', theta[1], '*x')

plt.show()