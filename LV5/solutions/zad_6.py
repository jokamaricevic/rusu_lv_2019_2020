# Zadatak 6
# Na temelju izraza pseudokoda iz Opisa vježbe implementirajte kmeans algoritam. Primijenite implementirani algoritam
# na podatke kao u Zadatku 1 te zaključite jeste li pravilno napravili implementaciju algoritma.
# Modificirajte programski kod tako da u svakoj iteraciji algoritma u polje pohranite i izračunate vrijednosti centara
# klastera. Primijenite programski kod na podatke kao u Zadatku 1 te prikažite u ravnini podatke i izračunate centre u
# svakoj iteraciji algoritma (npr. spojite izračunate vrijednosti centara linijama kako bi vizualizirali „kretanje“ centara
# kroz iteracije kmeans algoritma). 