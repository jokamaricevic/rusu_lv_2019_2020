# Zadatak 4
# Primijenite scikit-learn kmeans metodu za kvantizaciju boje na slici. Proučite kod 5.2. iz priloga vježbe te ga primijenite
# za kvantizaciju boje na slici example_grayscale.png koja se nalazi u
# rusu_lv_2019_20/LV5/resources/. Mijenjajte broj klastera. Što primjećujete? Izračunajte kolika se
# kompresija ove slike može postići ako se koristi 10 klastera.
# Pomoću sljedećeg koda možete učitati sliku:

import matplotlib.image as mpimg
import scipy as sp
from sklearn import cluster, datasets
import numpy as np
import matplotlib.pyplot as plt


imageNew = mpimg.imread('LV5\\resources\\example_grayscale.png')


    
X = imageNew.reshape((-1, 1)) # We need an (n_sample, n_feature) array
k_means = cluster.KMeans(n_clusters=10,n_init=1)
k_means.fit(X) 
values = k_means.cluster_centers_.squeeze()
labels = k_means.labels_
imageNew_compressed = np.choose(labels, values)
imageNew_compressed.shape = imageNew.shape

plt.figure(1)
plt.imshow(imageNew,  cmap='gray')

plt.figure(2)
plt.imshow(imageNew_compressed,  cmap='gray')


plt.show()