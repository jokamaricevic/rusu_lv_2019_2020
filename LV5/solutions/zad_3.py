# Zadatak 3
# Primijenite hijerarhijsko grupiranje na podatke korištene u Zadatku 1 pomoću funkcije linkage koja je ugrađena scipy
# metoda za agglomerative clustering:
# from scipy.cluster.hierarchy import dendrogram, linkage
# Prikažite pripadni dendogram. Mijenjajte korištenu metodu (argument method). Kako komentirate postignute rezultate? 

import funkcija_5_1
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.cluster import AgglomerativeClustering
from scipy.cluster.hierarchy import dendrogram, linkage 


data = funkcija_5_1.generate_data(30, 3)
scaler = StandardScaler()
scaled_features = scaler.fit_transform(data)

plt.figure()
cluster = AgglomerativeClustering(n_clusters=4, affinity = "euclidean", linkage="ward").fit(scaled_features)
cluster.fit_predict(scaled_features)

dendrogram(linkage(cluster.children_, method='ward'), no_plot=False)

plt.show()