import funkcija_5_1
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler

data = funkcija_5_1.generate_data(500, 3)

plt.figure()
scaler = StandardScaler()
scaled_features = scaler.fit_transform(data)


kmeans = KMeans(
    init="random",
    n_clusters=4,
    n_init=10,
    max_iter=300,
    random_state=42
    )
kmeans.fit(scaled_features)
y_kmeans = kmeans.predict(scaled_features)
plt.scatter(scaled_features[:, 0], scaled_features[:, 1], c=y_kmeans, s=50, cmap='viridis', marker=".")
plt.scatter(kmeans.cluster_centers_[:,0], kmeans.cluster_centers_[:,1], marker="x", s=100)
print(kmeans._n_threads)

plt.show()

