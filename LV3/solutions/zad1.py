import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('LV3\\resources\\mtcars.csv')

def spacer(zad):
    print("\n-----------------Zad 1." + zad + "-----------------")


# 1. Kojih 5 automobila ima najveću potrošnju? (koristite funkciju sort) 
spacer("1. Kojih 5 automobila ima najveću potrošnju? (koristite funkciju sort) ")
print(mtcars.sort_values(by=['mpg']).head(5)['car'])


# 2.  Koja tri automobila s 8 cilindara imaju najmanju potrošnju? 
spacer("2.  Koja tri automobila s 8 cilindara imaju najmanju potrošnju? ")
print(mtcars[(mtcars.cyl == 8)].sort_values(by=['mpg'], ascending=False).head(3)) 


# 3.  Kolika je srednja potrošnja automobila sa 6 cilindara?
spacer("3. Kolika je srednja potrošnja automobila sa 6 cilindara?")
print(mtcars[(mtcars.cyl == 6)].mean()['mpg'])


# 4. Kolika je srednja potrošnja automobila s 4 cilindra mase između 2000 i 2200 lbs?
spacer("4. Kolika je srednja potrošnja automobila s 4 cilindra mase između 2000 i 2200 lbs?")
print(mtcars[(mtcars.cyl == 4) & (mtcars.wt >= 2.000) & (mtcars.wt <= 2.200)].mean()['mpg'])


# 5. Koliko je automobila s ručnim, a koliko s automatskim mjenjačem u ovom skupu podataka?
spacer("5. Koliko je automobila s ručnim, a koliko s automatskim mjenjačem u ovom skupu podataka?")
print("Automatic:\t" + str(mtcars[(mtcars.am == 0)].count().car))
print("Manual:\t\t" + str(mtcars[(mtcars.am == 1)].count().car))


# 6. Koliko je automobila s automatskim mjenjačem i snagom preko 100 konjskih snaga?
spacer("6. Koliko je automobila s automatskim mjenjačem i snagom preko 100 konjskih snaga?")
print(mtcars[(mtcars.am == 0) & (mtcars.hp > 100)].count().car)


# 7. Kolika je masa svakog automobila u kilogramima? 
spacer("7. Kolika je masa svakog automobila u kilogramima? ")
mtcars_new = mtcars
mtcars_new['wt'] = mtcars_new['wt'].multiply(1/2.2)
print(mtcars_new[['car', 'wt']])