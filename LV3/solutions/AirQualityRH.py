import urllib.request
import matplotlib.pyplot as plt
import pandas as pd
import xml.etree.ElementTree as ET


url = 'http://iszz.azo.hr/iskzl/rs/podatak/export/xml?postaja=160&polutant=5&tipPodatka=4&vrijemeOd=1.1.2017&vrijemeDo=31.12.2017'

airQualityHR = urllib.request.urlopen(url).read()
root = ET.fromstring(airQualityHR)

df = pd.DataFrame(columns=('mjerenje', 'vrijeme'))

i = 0
while True:
    
    try:
        obj = list(list(root)[i])
    except:
        break
    
    row = dict(zip(['mjerenje', 'vrijeme'], [obj[0].text, obj[2].text]))
    row_s = pd.Series(row)
    row_s.name = i
    df = df.append(row_s)
    df.mjerenje[i] = float(df.mjerenje[i])
    i = i + 1


df.vrijeme = pd.to_datetime(df.vrijeme, utc=True)
# df.plot(y='mjerenje', x='vrijeme')

# add date month and day designator
df['month'] = df['vrijeme'].dt.month
df['dayOfweek'] = df['vrijeme'].dt.dayofweek

# zad 2.
# print(df.sort_values(by=['mjerenje'], ascending = False).head(3))


# zad 4.
winterMonth = df[df.month == 2]
summerMonth = df[df.month == 7]

plt.bar(range(len(winterMonth)), winterMonth.mjerenje)
plt.bar(range(len(summerMonth)), summerMonth.mjerenje)
plt.xticks(rotation=90)

plt.show()