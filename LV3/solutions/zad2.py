import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('LV3\\resources\\mtcars.csv')


# zad 1.
# cyl4 = mtcars[(mtcars.cyl == 4)]
# cyl6 = mtcars[(mtcars.cyl == 6)]
# cyl8 = mtcars[(mtcars.cyl == 8)]

# plt.bar(cyl4.car, cyl4.mpg)
# plt.bar(cyl6.car, cyl6.mpg)
# plt.bar(cyl8.car, cyl8.mpg)
# plt.legend(labels=['cyl4', 'cyl6', 'cyl8'])
# plt.xticks(rotation=90)

# plt.show()

# zad 2.
cyl4 = mtcars[(mtcars.cyl == 4)]
cyl6 = mtcars[(mtcars.cyl == 6)]
cyl8 = mtcars[(mtcars.cyl == 8)]

plt.boxplot([cyl4.wt, cyl6.wt, cyl8.wt] )

plt.show()

# zad 3.
# automatic = mtcars[(mtcars.am == 0)]
# manual = mtcars[(mtcars.am == 1)]

# plt.bar(automatic.car, automatic.mpg)
# plt.bar(manual.car, manual.mpg)
# plt.legend(labels=['automatic', 'manual'])
# plt.xticks(rotation=90)

# plt.show()

# zad 4.
# automatic = mtcars[(mtcars.am == 0)]
# manual = mtcars[(mtcars.am == 1)]


# plt.figure()
# plt.scatter(automatic.qsec, automatic.hp, marker='o')   
# plt.scatter(manual.qsec, manual.hp, marker='s')  
# plt.ylabel('Snaga')
# plt.xlabel('Ubrzanje')
# plt.legend(["automatic","manual"])
# plt.grid()

# plt.show()