def total_kn(workHours, payPerHour):
    total = workHours * payPerHour
    return total

workHours = float(input('Unesite broj radnih sati: '))
payPerHour = float(input('Unesite cijenu za jedan radni sat: '))
total = total_kn(workHours, payPerHour)
print(
    '''\tRadni sati: {}h
\tkn/h: {}
\tUkupno: {}kn'''.format(workHours, payPerHour, total ))