fileName = input("Unesite put do datoteke: ")
try:
    fhand = open(fileName)
except:
    print ('File cannot be opened: ', fileName)
    exit()

values = []

for line in fhand:
    if "X-DSPAM-Confidence:" in line:
        lineSplit = line.split(" ")
        values.append(float(lineSplit[1].strip()))

avg = sum(values) / len(values)
name = fileName.split("\\")[-1]

print("File name: ", name)
print("Average X-DSPAM-Confidence: ", avg)