grade = float(input('Unesite ocijenu: '))
while grade < 0.0 or grade > 1.0:
    grade = float(input('Pogresan unos: '))
sgrade = ""
if grade >= 0.9:
    sgrade = "A"
elif grade >= 0.8:
    sgrade = "B"
elif grade >= 0.7:
    sgrade = "C"
elif grade >= 0.6:
    sgrade = "D"
else:
    sgrade = "F"

print("Ocijena je {}.".format(sgrade))